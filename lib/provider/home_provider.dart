import 'package:flutter/material.dart';

class HomeProvider extends ChangeNotifier {
  int selectedTabIndex = 0;
  List<String> tabBarList = [
    "Arbeitnehmer",
    "Arbeitgeber",
    "Temporärbüro",
  ];

  /// Change the selected tab index and notify listeners.
  ///
  /// The [index] parameter specifies the new selected tab index.
  void changeTabIndex(int index) {
    // Update the selected tab index.
    selectedTabIndex = index;

    // Notify listeners that the state of the provider has changed.
    notifyListeners();
  }
}
