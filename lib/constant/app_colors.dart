import 'package:flutter/material.dart';

const Color appPrimaryColor = Color(0xFF3182CE);
const Color appBackgroundColor = Color(0xFFEBF4FF);
const Color whiteColor = Color(0xFFFFFFFF);
const Color blackColor = Color(0XFF2D3748);
const Color appTextColor = Color(0XFF319795);
const Color selectedTabIndex = Color(0XFFE6FFFA);
const Color tabBarColor = Color(0XFF81E6D9);
const Color borderColor = Color(0XFFCBD5E0);
const Color grayColor = Color(0XFF718096);
