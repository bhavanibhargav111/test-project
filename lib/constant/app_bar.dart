import 'package:flutter/material.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';

AppBar appBar() {
  return AppBar(
    shape: const RoundedRectangleBorder(
      borderRadius: BorderRadius.vertical(
        bottom: Radius.circular(12),
      ),
    ),
    toolbarHeight: 8,
    backgroundColor: appPrimaryColor,
    bottom: PreferredSize(
      preferredSize: const Size.fromHeight(67),
      child: Container(
        height: 67,
        decoration: BoxDecoration(
          borderRadius: const BorderRadius.vertical(
            bottom: Radius.circular(12),
          ),
          color: whiteColor,
          boxShadow: [
            BoxShadow(
              color: blackColor.withOpacity(0.13),
              offset: const Offset(0, 1),
              spreadRadius: 3,
              blurRadius: 6,
            )
          ],
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            TextButton(
              onPressed: () {},
              child: Text(
                "Login",
                style: AppTextStyle.normalRegularBold15.copyWith(
                  fontWeight: FontWeight.w600,
                  color: appTextColor,
                ),
              ),
            )
          ],
        ),
      ),
    ),
  );
}
