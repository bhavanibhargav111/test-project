import 'package:flutter/material.dart';
import 'package:test_project/my_app.dart';

/// The entry point of the Flutter application.
///
/// This function is where the application starts executing. It is the main()
/// function that is called when the application is launched. It initializes
/// the Flutter framework and sets the root of the application widget tree to
/// be a [MyApp] widget.
void main() {
  // Run the app and mark the tree as being unchangeable (i.e. "const").
  runApp(const MyApp());
}
