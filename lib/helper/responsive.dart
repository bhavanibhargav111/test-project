import 'package:flutter/material.dart';

class ResponsiveView extends StatelessWidget {
  const ResponsiveView({
    super.key,
    required this.mobile,
    required this.desktop,
  });

  final Widget mobile;
  final Widget desktop;

  /// Determines whether the screen size is considered 'mobile' based on
  /// the width of the screen.
  ///
  /// The screen size is considered 'mobile' if its width is less than 850
  /// logical pixels.
  ///
  /// Parameters:
  ///   context: The [BuildContext] used to retrieve the screen size.
  ///
  /// Returns:
  ///   A boolean indicating whether the screen size is considered 'mobile'.
  static bool isMobile(BuildContext context) {
    // Retrieve the screen size from the provided context.
    final Size screenSize = MediaQuery.of(context).size;

    // Determine if the screen size is considered 'mobile' by checking if
    // its width is less than 850 logical pixels.
    return screenSize.width < 850;
  }

  /// Determines whether the screen size is considered 'desktop' based on
  /// the width of the screen.
  ///
  /// The screen size is considered 'desktop' if its width is greater than or
  /// equal to 1100 logical pixels.
  ///
  /// Parameters:
  ///   context: The [BuildContext] used to retrieve the screen size.
  ///
  /// Returns:
  ///   A boolean indicating whether the screen size is considered 'desktop'.
  static bool isDesktop(BuildContext context) {
    // Retrieve the screen size from the provided context.
    //
    // The screen size is considered 'desktop' if its width is greater than or
    // equal to 1100 logical pixels.
    final Size screenSize = MediaQuery.of(context).size;

    // Determine if the screen size is considered 'desktop' by checking if
    // its width is greater than or equal to 1100 logical pixels.
    return screenSize.width >= 1100;
  }

  @override

  /// Builds the widget tree based on the screen size.
  ///
  /// The [desktop] widget is returned if the screen width is greater than or
  /// equal to 1100 logical pixels. Otherwise, if the screen width is greater
  /// than or equal to 850 logical pixels, the [tablet] widget is returned.
  /// Otherwise, the [mobile] widget is returned.
  ///
  /// Parameters:
  ///   context: The [BuildContext] used to retrieve the screen size.
  ///
  /// Returns:
  ///   The appropriate widget based on the screen size.
  Widget build(BuildContext context) {
    return LayoutBuilder(
      builder: (context, constraints) {
        // Check the screen width and return the corresponding widget.
        if (constraints.maxWidth >= 1100) {
          // If the screen width is greater than or equal to 1100 logical pixels,
          // return the desktop widget.
          return desktop;
        } else if (constraints.maxWidth >= 850) {
          // If the screen width is greater than or equal to 850 logical pixels,
          // return the desktop widget.
          return desktop;
        } else {
          // If the screen width is less than 850 logical pixels, return the
          // mobile widget.
          return mobile;
        }
      },
    );
  }
}
