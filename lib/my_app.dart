import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_project/provider/home_provider.dart';
import 'package:test_project/screen/home_screen/home_screen_view.dart';

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override

  /// Builds the app widget tree.
  ///
  /// This is the entry point of the app. It creates a ChangeNotifierProvider
  /// that provides a [HomeProvider] instance and wraps the app in a
  /// [MaterialApp]. The [MaterialApp] is configured with a title, a theme,
  /// and a home widget.
  ///
  /// Returns a [MaterialApp] widget.
  Widget build(BuildContext context) {
    // Build the app widget tree.
    return ChangeNotifierProvider(
      // Create a ChangeNotifierProvider that provides a HomeProvider instance.
      create: (context) => HomeProvider(),
      // Wrap the app in a MaterialApp.
      child: MaterialApp(
        // Set the app title.
        title: 'Test Project',
        // Hide the debug banner.
        debugShowCheckedModeBanner: false,
        // Set the app theme.
        theme: ThemeData(
          // Set the app color scheme.
          colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
          // Enable Material 3.
          useMaterial3: true,
        ),
        // Set the home widget.
        home: const HomeScreen(),
      ),
    );
  }
}
