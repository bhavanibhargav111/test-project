import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';

class ArbeitnehmerDesktopView extends StatefulWidget {
  const ArbeitnehmerDesktopView({super.key});

  @override
  State<ArbeitnehmerDesktopView> createState() =>
      _ArbeitnehmerDesktopViewState();
}

class _ArbeitnehmerDesktopViewState extends State<ArbeitnehmerDesktopView> {
  @override

  /// Builds the main layout of the ArbeitnehmerDesktopView.
  ///
  /// This function builds a column with a centered cross and main axis alignment.
  /// It also calculates the width and height of the device's screen and uses
  /// them to set the padding and size of various widgets.
  Widget build(BuildContext context) {
    // Calculate the width and height of the device's screen.
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    return Column(
      crossAxisAlignment:
          CrossAxisAlignment.center, // Set the cross axis alignment to center.
      mainAxisAlignment:
          MainAxisAlignment.center, // Set the main axis alignment to center.
      children: [
        // Add a padding with symmetric horizontal padding of 20.0 and a centered Text widget.
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            "Drei einfache Schritte zu deinem neuen Job", // Set the text of the Text widget.
            style: AppTextStyle.normalRegularBold20.copyWith(
              color: grayColor, // Set the color of the Text widget.
            ),
            textAlign: TextAlign.center, // Set the text alignment to center.
          ),
        ),
        height20, // Add a height of 20.0.
        Padding(
          padding: EdgeInsets.only(
              right:
                  w * 0.3), // Set the right padding of 30% of the screen width.
          child: _stepOne(context), // Add the first step widget.
        ),
        Padding(
          padding: EdgeInsets.only(
              right:
                  w * 0.3), // Set the right padding of 30% of the screen width.
          child: Image.asset(
            "assets/png/arrow1.png", // Set the image asset path.
            width: w * 0.6, // Set the width of 60% of the screen width.
            height: h * 0.4, // Set the height of 40% of the screen height.
          ),
        ),
        _stepTwo(context), // Add the second step widget.
        Padding(
          padding: EdgeInsets.only(
              right:
                  w * 0.2), // Set the right padding of 20% of the screen width.
          child: Image.asset(
            "assets/png/arrow2.png", // Set the image asset path.
            width: w * 0.7, // Set the width of 70% of the screen width.
            height: h * 0.3, // Set the height of 30% of the screen height.
          ),
        ),
        Padding(
          padding: EdgeInsets.only(
              right: w *
                  0.25), // Set the right padding of 25% of the screen width.
          child: _stepThree(context), // Add the third step widget.
        ),
      ],
    );
  }

  /// Widget for the third step of the ArbeitnehmerDesktopView.
  ///
  /// This widget displays a row with a RichText widget and an Image widget.
  /// The RichText widget displays the step number and step description,
  /// while the Image widget displays an image asset.
  ///
  /// Parameters:
  /// - [context]: The build context of the widget.
  Widget _stepThree(BuildContext context) {
    // The height of the SizedBox widget.
    double height = 200;

    return SizedBox(
      height: height,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // RichText widget displaying the step number and description.
          RichText(
            text: TextSpan(
              text: "3. ",
              style: AppTextStyle.normalRegularBold42
                  .copyWith(fontSize: 130, color: grayColor),
              children: [
                TextSpan(
                  text: "Erstellen dein Lebenslauf",
                  style:
                      AppTextStyle.normalSemiBold15.copyWith(color: grayColor),
                )
              ],
            ),
          ),
          customWidth(30),
          // Image widget displaying an image asset.
          Image.asset(
            "assets/png/arbeitnehmer_step_three.png",
            height: 144,
          ),
        ],
      ),
    );
  }

  /// Widget for the first step of the ArbeitnehmerDesktopView.
  ///
  /// This widget displays a row with a RichText widget and an Image widget.
  /// The RichText widget displays the step number and step description,
  /// while the Image widget displays an image asset.
  ///
  /// Parameters:
  /// - [context]: The build context of the widget.
  Widget _stepOne(BuildContext context) {
    // The height of the SizedBox widget.
    const double height = 200.0;

    return SizedBox(
      height: height,
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // RichText widget displaying the step number and description.
          RichText(
            text: TextSpan(
              text: "1. ",
              style: AppTextStyle.normalRegularBold42
                  .copyWith(fontSize: 130, color: grayColor),
              children: [
                TextSpan(
                  text: "Erstellen dein Lebenslauf",
                  style:
                      AppTextStyle.normalSemiBold15.copyWith(color: grayColor),
                )
              ],
            ),
          ),
          customWidth(30),
          // Image widget displaying an image asset.
          Image.asset(
            "assets/png/arbeitnehmer_step_one.png",
            height: 144,
          ),
        ],
      ),
    );
  }

  /// Widget for the second step of the ArbeitnehmerDesktopView.
  ///
  /// This widget displays a column with two Container widgets and an Image widget.
  /// The first Container widget is a gradient background, while the second Container
  /// widget contains an Image widget and a RichText widget. The Image widget displays
  /// an image asset, and the RichText widget displays the step number and step description.
  ///
  /// Parameters:
  /// - [context]: The build context of the widget.
  Widget _stepTwo(BuildContext context) {
    // Column widget to hold the different widgets.
    return Column(
      crossAxisAlignment: CrossAxisAlignment
          .center, // Align children in the center of the column.
      mainAxisAlignment: MainAxisAlignment
          .center, // Align children in the center of the column.
      mainAxisSize: MainAxisSize.min, // Make the column as small as possible.
      children: [
        ClipPath(
          clipper: WaveClipperOne(reverse: true), // Define the clipping path.
          child: Container(
            height: 50, // Height of the container.
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  appTextColor.withOpacity(0.2), // Colors of the gradient.
                  appPrimaryColor.withOpacity(0.2),
                ],
              ),
            ),
          ),
        ),
        // Container with an image asset and a rich text widget.
        _containerWithImageAndRichText(context),
        // Gradient background container.
        ClipPath(
          clipper: WaveClipperOne(), // Define the clipping path.
          child: Container(
            height: 50, // Height of the container.
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  appTextColor.withOpacity(0.2), // Colors of the gradient.
                  appPrimaryColor.withOpacity(0.2),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  // Gradient background container.
  Widget _gradientBackgroundContainer(BuildContext context) {
    return ClipPath(
      clipper: WaveClipperOne(reverse: true), // Define the clipping path.
      child: Container(
        height: 50, // Height of the container.
        decoration: BoxDecoration(
          gradient: LinearGradient(
            colors: [
              appTextColor.withOpacity(0.2), // Colors of the gradient.
              appPrimaryColor.withOpacity(0.2),
            ],
          ),
        ),
      ),
    );
  }

  // Container with an image asset and a rich text widget.
  Widget _containerWithImageAndRichText(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            appTextColor.withOpacity(0.2), // Colors of the gradient.
            appPrimaryColor.withOpacity(0.2),
          ],
        ),
      ),
      height: 150, // Height of the container.
      child: Row(
        mainAxisAlignment: MainAxisAlignment
            .center, // Align children in the center of the row.
        crossAxisAlignment: CrossAxisAlignment
            .center, // Align children in the center of the row.
        children: [
          Image.asset(
            "assets/png/arbeitnehmer_step_two.png", // Image asset path.
            height: 144, // Height of the image.
          ),
          customWidth(30),
          _richTextWidget(context), // Rich text widget.
        ],
      ),
    );
  }

  // Rich text widget.
  /// Widget for the rich text widget.
  ///
  /// This widget displays a text span with a step number, style, and children.
  /// The step number and description are the children of the text span.
  ///
  /// Parameters:
  /// - [context]: The build context of the widget.
  Widget _richTextWidget(BuildContext context) {
    return RichText(
      text: TextSpan(
        // TextSpan for the step number and step description.
        text: "2. ", // Step number.
        style: AppTextStyle.normalRegularBold42
            .copyWith(fontSize: 130, color: grayColor), // Style of the text.
        children: [
          TextSpan(
            text: "Erstellen dein Lebenslauf", // Step description.
            style: AppTextStyle.normalSemiBold15
                .copyWith(color: grayColor), // Style of the text.
          ),
        ],
      ),
    );
  }
}
