import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';

class TemporarburoDesktopView extends StatelessWidget {
  const TemporarburoDesktopView({super.key});

  @override

  /// Builds the UI for the TemporarburoDesktopView widget.
  ///
  /// This function takes the [BuildContext] and returns a [Column] widget
  /// containing multiple [Padding] and [Image] widgets. The UI consists of
  /// a text, two images, and a step three widget.
  Widget build(BuildContext context) {
    // Get the width and height of the screen
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Display a text
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            "Drei einfache Schritte zu deinem neuen Mitarbeiter",
            style: AppTextStyle.normalRegularBold20.copyWith(
              color: grayColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        height20,
        // Display the first step widget
        Padding(
          padding: EdgeInsets.only(right: w * 0.3),
          child: _stepOne(context),
        ),
        // Display the first arrow image
        Padding(
          padding: EdgeInsets.only(right: w * 0.3),
          child: Image.asset(
            "assets/png/arrow1.png",
            width: w * 0.6,
            height: h * 0.4,
          ),
        ),
        // Display the second step widget
        _stepTwo(context),
        // Display the second arrow image
        Padding(
          padding: EdgeInsets.only(right: w * 0.25),
          child: Image.asset(
            "assets/png/arrow2.png",
            width: w * 0.7,
            height: h * 0.3,
          ),
        ),
        // Display the third step widget
        Padding(
          padding: EdgeInsets.only(right: w * 0.25),
          child: _stepThree(context),
        ),
        customHeight(40)
      ],
    );
  }

  /// Displays the first step widget.
  ///
  /// This widget consists of a [RichText] widget with a text span that
  /// includes a "1. " at the beginning and the text
  /// "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter" in a
  /// different style. Also includes an image asset of the step one.
  Widget _stepOne(BuildContext context) {
    // The widget is created using a SizedBox with a fixed height of 200.
    // Inside the SizedBox, there is a Row widget that contains a
    // [RichText] widget and an [Image] widget.

    return SizedBox(
      height: 200,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          RichText(
            // The text span consists of "1. " and "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter".
            // The "1. " is displayed in a bold style with a font size of 130 and gray color.
            // The "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter" is displayed in a semi bold style with a font size of 15 and gray color.
            text: TextSpan(
              text: "1. ",
              style: AppTextStyle.normalRegularBold42
                  .copyWith(fontSize: 130, color: grayColor),
              children: [
                TextSpan(
                  text:
                      "Drei einfache Schritte zur Vermittlung neuer Mitarbeiter",
                  style:
                      AppTextStyle.normalSemiBold15.copyWith(color: grayColor),
                ),
              ],
            ),
          ),
          customWidth(30),
          // Displays an image asset of the step one.
          // The height of the image is set to 144.
          Image.asset(
            "assets/png/arbeitnehmer_step_one.png",
            height: 144,
          ),
        ],
      ),
    );
  }

  /// Displays the second step widget.
  ///
  /// This widget consists of a column that contains three widgets. The first
  /// widget is a [ClipPath] that clips the container to a wave shape. The
  /// second widget is a container with a gradient and an image and text widget.
  /// The third widget is a [ClipPath] that clips the container to a wave shape.
  Widget _stepTwo(BuildContext context) {
    // The widget is created using a Column widget that contains three widgets.
    // The first widget is a ClipPath that clips the container to a wave shape
    // with a reverse parameter of true. The second widget is a container with
    // a gradient and an image and text widget. The third widget is a ClipPath
    // that clips the container to a wave shape.

    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        // The first widget, a ClipPath that clips the container to a wave shape.
        ClipPath(
          clipper: WaveClipperOne(reverse: true),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  appTextColor.withOpacity(0.2),
                  appPrimaryColor.withOpacity(0.2),
                ],
              ),
            ),
          ),
        ),
        // The second widget, a container with a gradient and an image and text widget.
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                appTextColor.withOpacity(0.2),
                appPrimaryColor.withOpacity(0.2),
              ],
            ),
          ),
          height: 150,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/png/temporarburoView_step_two.png",
                height: 150,
              ),
              customWidth(30),
              RichText(
                text: TextSpan(
                  text: "2. ",
                  style: AppTextStyle.normalRegularBold42
                      .copyWith(fontSize: 130, color: grayColor),
                  children: [
                    TextSpan(
                      text: "Erhalte Vermittlungs- angebot von Arbeitgeber",
                      style: AppTextStyle.normalSemiBold15
                          .copyWith(color: grayColor),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        // The third widget, a ClipPath that clips the container to a wave shape.
        ClipPath(
          clipper: WaveClipperOne(),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  appTextColor.withOpacity(0.2),
                  appPrimaryColor.withOpacity(0.2),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

/// Widget representing the third step of the Temporärbüro desktop view.
///
/// This widget is a [Padding] widget that wraps a [SizedBox] with a fixed
/// height and a [Row] widget. The [Padding] widget adds horizontal padding
/// of 20.0 to the [SizedBox]. The [SizedBox] has a height of 200 and a
/// [Row] widget as its child. The [Row] widget has a crossAxisAlignment of
/// center and a mainAxisAlignment of center. It contains three widgets: a
/// [RichText] widget, a [customWidth] widget, and an [Image] widget.
Widget _stepThree(BuildContext context) {
  // The widget is created using a Padding widget that wraps a SizedBox with
  // a fixed height and a Row widget.
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20.0),
    child: SizedBox(
      height: 200, // The height of the SizedBox.
      child: Row(
        crossAxisAlignment: CrossAxisAlignment
            .center, // The alignment of the children along the cross axis.
        mainAxisAlignment: MainAxisAlignment
            .center, // The alignment of the children along the main axis.
        children: [
          RichText(
            text: TextSpan(
              text: "3. ", // The text displayed before the main text.
              style: AppTextStyle.normalRegularBold42.copyWith(
                  fontSize: 130, color: grayColor), // The style of the text.
              children: [
                TextSpan(
                  text: "Mit nur einem Klick bewerben", // The main text.
                  style: AppTextStyle.normalSemiBold15.copyWith(
                      color: grayColor), // The style of the main text.
                ),
              ],
            ),
          ), // The RichText widget.
          customWidth(30), // The customWidth widget.
          Image.asset(
            "assets/png/temporarburoView_step_three.png", // The asset of the Image widget.
            height: 189, // The height of the Image widget.
          ), // The Image widget.
        ],
      ),
    ),
  );
}
