import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';

class ArbeitgeberDesktopView extends StatelessWidget {
  const ArbeitgeberDesktopView({super.key});

  @override

  /// Builds the main layout of the ArbeitgeberDesktopView.
  ///
  /// This widget builds a column with the main layout of the view.
  /// It includes a text with instructions, and three steps represented by
  /// [_stepOne], [_stepTwo] and [_stepThree] widgets.
  /// Each step is padded with appropriate offsets.
  /// The [_stepTwo] and [_stepThree] widgets are also accompanied by an image.
  @override
  Widget build(BuildContext context) {
    // Get the width and height of the screen.
    double w = MediaQuery.of(context).size.width;
    double h = MediaQuery.of(context).size.height;

    return Column(
      // Set the cross and main axis alignments to center.
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Add padding to the text widget and include the instructions.
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Text(
            "Drei einfache Schritte zu deinem neuen Mitarbeiter",
            style: AppTextStyle.normalRegularBold20.copyWith(
              color: grayColor,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        // Add spacing of 20 pixels.
        height20,
        // Add padding to the step one widget.
        Padding(
          padding: EdgeInsets.only(right: w * 0.3),
          child: _stepOne(context),
        ),
        // Add image and padding to the step one widget.
        Padding(
          padding: EdgeInsets.only(right: w * 0.3),
          child: Image.asset(
            "assets/png/arrow1.png",
            width: w * 0.6,
            height: h * 0.4,
          ),
        ),
        // Add step two widget.
        _stepTwo(context),
        // Add image and padding to the step two widget.
        Padding(
          padding: EdgeInsets.only(right: w * 0.2),
          child: Image.asset(
            "assets/png/arrow2.png",
            width: w * 0.7,
            height: h * 0.3,
          ),
        ),
        // Add step three widget.
        Padding(
          padding: EdgeInsets.only(right: w * 0.25),
          child: _stepThree(context),
        ),
        // Add spacing of 40 pixels.
        customHeight(40)
      ],
    );
  }

  /// A widget that displays the first step of the Arbeitgeber desktop screen.
  ///
  /// This widget is a part of the Arbeitgeber desktop screen and is displayed
  /// vertically with a [Row] widget. It consists of a [RichText] widget that
  /// displays the number "1." followed by the text "Erstellen dein
  /// Unternehmensprofil". The number is styled with [AppTextStyle.normalRegularBold42]
  /// and the text is styled with [AppTextStyle.normalSemiBold15]. The widget is
  /// wrapped in a [SizedBox] with a fixed height of 200 and a width that
  /// expands to fill the available space. The widget is also a part of a
  /// [Row] widget that is centered both horizontally and vertically.
  Widget _stepOne(BuildContext context) {
    return SizedBox(
      height: 200,
      width: double.infinity,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          // Display the number "1." styled as a heading with a gray color.
          RichText(
            text: TextSpan(
              text: "1. ",
              style: AppTextStyle.normalRegularBold42
                  .copyWith(fontSize: 130, color: grayColor),
              children: [
                // Display the text "Erstellen dein Unternehmensprofil" styled
                // as a body text with a gray color.
                TextSpan(
                  text: "Erstellen dein Unternehmensprofil",
                  style:
                      AppTextStyle.normalSemiBold15.copyWith(color: grayColor),
                )
              ],
            ),
          ),
          // Add a horizontal spacing of 30 pixels.
          customWidth(30),
          // Display an image with a fixed height of 144 pixels.
          Image.asset(
            "assets/png/arbeitnehmer_step_one.png",
            height: 144,
          ),
        ],
      ),
    );
  }

  /// A widget that displays the second step of the Arbeitgeber desktop screen.
  ///
  /// This widget is a part of the Arbeitgeber desktop screen and is displayed
  /// vertically with a [Column] widget. It consists of three child widgets:
  /// [ClipPath], [Container], and [ClipPath]. The first and third child widgets
  /// are used to create the wave effect at the top and bottom of the widget.
  /// The second child widget is a [Container] widget that displays an image
  /// and a text. The image is displayed with a fixed height of 189 pixels and
  /// the text is styled with [AppTextStyle.normalRegularBold42] and
  /// [AppTextStyle.normalSemiBold15]. The container is wrapped in a
  /// [Row] widget that is centered both horizontally and vertically.
  Widget _stepTwo(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      mainAxisSize: MainAxisSize.min,
      children: [
        // Create a wave effect at the top of the widget.
        ClipPath(
          clipper: WaveClipperOne(reverse: true),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  appTextColor.withOpacity(0.2),
                  appPrimaryColor.withOpacity(0.2),
                ],
              ),
            ),
          ),
        ),
        // Display an image and a text in a container.
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                appTextColor.withOpacity(0.2),
                appPrimaryColor.withOpacity(0.2),
              ],
            ),
          ),
          height: 150,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Image.asset(
                "assets/png/arbeitgeber_step_two.png",
                height: 189,
              ),
              customWidth(30),
              // Display the number "2." styled as a heading with a gray color.
              RichText(
                text: TextSpan(
                  text: "2. ",
                  style: AppTextStyle.normalRegularBold42
                      .copyWith(fontSize: 130, color: grayColor),
                  children: [
                    // Display the text "Erstellen dein Lebenslauf" styled
                    // as a body text with a gray color.
                    TextSpan(
                      text: "Erstellen dein Lebenslauf",
                      style: AppTextStyle.normalSemiBold15
                          .copyWith(color: grayColor),
                    )
                  ],
                ),
              ),
            ],
          ),
        ),
        // Create a wave effect at the bottom of the widget.
        ClipPath(
          clipper: WaveClipperOne(),
          child: Container(
            height: 50,
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors: [
                  appTextColor.withOpacity(0.2),
                  appPrimaryColor.withOpacity(0.2),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}

/// Widget representing the third step of the Arbeitgeber desktop screen.
///
/// This widget is a [SizedBox] widget that contains a [Row] widget. It
/// displays the text "3. Wähle deinen neuen Mitarbeiter aus" styled as a
/// heading and an image. The text is displayed using a [RichText] widget,
/// while the image is displayed using an [Image.asset] widget. The widget
/// is 200 pixels in height and the text and the image are centered within
/// the row.
Widget _stepThree(BuildContext context) {
  return SizedBox(
    height: 200,
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        // Display the number "3." styled as a heading with a gray color.
        RichText(
          text: TextSpan(
            text: "3. ",
            style: AppTextStyle.normalRegularBold42
                .copyWith(fontSize: 130, color: grayColor),
            children: [
              // Display the text "Wähle deinen neuen Mitarbeiter aus" styled
              // as a body text with a gray color.
              TextSpan(
                text: "Wähle deinen neuen Mitarbeiter aus",
                style: AppTextStyle.normalSemiBold15.copyWith(color: grayColor),
              ),
            ],
          ),
        ),
        customWidth(30),

        // Display an image.
        Image.asset(
          "assets/png/arbeitgeber_step_three.png",
          height: 189,
        ),
      ],
    ),
  );
}
