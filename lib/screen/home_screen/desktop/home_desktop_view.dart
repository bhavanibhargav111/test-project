import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';
import 'package:test_project/constant/app_bar.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';
import 'package:test_project/provider/home_provider.dart';
import 'tab_bar_view/arbeitgeber_desktop_view.dart';
import 'tab_bar_view/arbeitnehmer_desktop_view.dart';
import 'tab_bar_view/temporärbüro_desktop_view.dart';

class HomeDesktopView extends StatefulWidget {
  const HomeDesktopView({super.key});

  @override
  State<HomeDesktopView> createState() => _HomeDesktopViewState();
}

class _HomeDesktopViewState extends State<HomeDesktopView> {
  @override

  /// Builds the HomeDesktopView widget.
  ///
  /// The [build] method builds the Scaffold widget which contains the appBar,
  /// body and child. The appBar is set to [appBar]. The body is set to a
  /// Consumer widget that builds a SingleChildScrollView widget. The child
  /// is a Column widget that contains [_headerView], height10, Padding widget
  /// and the child is a SingleChildScrollView widget. The child of the
  /// SingleChildScrollView widget is a Row widget that contains a list of
  /// GestureDetector widgets. Each GestureDetector widget contains a Container
  /// widget. The child of the Container widget is a Text widget. The body is
  /// selected based on the value of selectedTabIndex in HomeProvider.
  Widget build(BuildContext context) {
    return Scaffold(
      // The appBar of the Scaffold widget.
      appBar: appBar(),
      // The body of the Scaffold widget.
      body: Consumer<HomeProvider>(
        // Builds the body of the Scaffold widget.
        builder: (context, value, child) {
          // Builds a SingleChildScrollView widget that contains a Column widget.
          return SingleChildScrollView(
            child: Column(
              // Builds the children of the Column widget.
              children: [
                // Builds the header view.
                _headerView(),
                // Adds 20 vertical space.
                height10,
                // Builds the padding widget.
                Padding(
                  // The padding of the Padding widget.
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  // Builds the SizedBox widget.
                  child: SizedBox(
                    // The height of the SizedBox widget.
                    height: 50,
                    // Builds the SingleChildScrollView widget.
                    child: SingleChildScrollView(
                      // The scroll direction of the SingleChildScrollView widget.
                      scrollDirection: Axis.horizontal,
                      // Builds the Row widget.
                      child: Row(
                        // Builds the list of GestureDetector widgets.
                        children: [
                          // Builds a list of GestureDetector widgets based on
                          // the length of tabBarList.
                          ...List.generate(
                            value.tabBarList.length,
                            // Builds a GestureDetector widget.
                            (index) => GestureDetector(
                              // Called when the GestureDetector is tapped.
                              onTap: () {
                                // Calls the changeTabIndex method of HomeProvider.
                                value.changeTabIndex(index);
                              },
                              // Builds a Container widget.
                              child: Container(
                                // The decoration of the Container widget.
                                decoration: BoxDecoration(
                                  // The border radius of the Container widget.
                                  borderRadius: BorderRadius.horizontal(
                                    // The left radius of the Container widget.
                                    left: Radius.circular(
                                      index == 0 ? 12 : 0,
                                    ),
                                    // The right radius of the Container widget.
                                    right: Radius.circular(
                                      index == 2 ? 12 : 0,
                                    ),
                                  ),
                                  // The border of the Container widget.
                                  border: Border.all(
                                    color: value.selectedTabIndex == index
                                        ? tabBarColor
                                        : borderColor,
                                  ),
                                  // The color of the Container widget.
                                  color: value.selectedTabIndex == index
                                      ? tabBarColor
                                      : Colors.transparent,
                                ),
                                // The height of the Container widget.
                                height: 40,
                                // The width of the Container widget.
                                width: 160,
                                // Builds the Center widget.
                                child: Center(
                                  // Builds a Text widget.
                                  child: Text(
                                    // The text of the Text widget.
                                    value.tabBarList[index],
                                    // The style of the Text widget.
                                    style: AppTextStyle.regularBold15.copyWith(
                                      // The color of the Text widget.
                                      color: value.selectedTabIndex == index
                                          ? selectedTabIndex
                                          : appTextColor,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                // Adds 20 vertical space.
                customHeight(20),
                // Builds the child widget based on the selectedTabIndex.
                _buildTabView(value),
                // Adds 20 vertical space.
                height20
              ],
            ),
          );
        },
      ),
    );
  }

  /// Builds the tab view based on the selected tab index.
  ///
  /// Returns one of three tab views based on the selected tab index.
  Widget _buildTabView(HomeProvider value) {
    // Build the tab view.
    return [
      const ArbeitnehmerDesktopView(),
      const ArbeitgeberDesktopView(),
      const TemporarburoDesktopView(),
    ][value.selectedTabIndex];
  }

  /// Builds the header view of the HomeDesktopView.
  ///
  /// Returns a [ClipPath] widget that clips a [Container] with a [BoxDecoration]
  /// gradient and a [Row] widget containing a [Column] widget and a [CircleAvatar] widget.
  /// The [Column] widget contains a [Text] widget and a vertical space. The [CircleAvatar]
  /// widget contains an [SvgPicture] widget.
  ///
  /// The [ClipPath] widget clips the [Container] widget using the [WaveClipperTwo] clipper.
  /// The [Container] widget has a height of 500 and a [LinearGradient] decoration.
  /// The [Row] widget has a crossAxisAlignment of center and a mainAxisAlignment of center.
  /// The [Column] widget has a mainAxisAlignment of center. The [Text] widget displays the
  /// "Deine Job\nwebsite" text with a font size of 65 and a blackColor. The vertical space
  /// is 10 pixels. The [CircleAvatar] widget has a radius of 150, a whiteColor background,
  /// and a [SvgPicture] widget with a fit of BoxFit.cover and an alignment of Alignment.topCenter.
  Widget _headerView() {
    return ClipPath(
      // Clips the Container widget using WaveClipperTwo
      clipper: WaveClipperTwo(),
      child: Container(
        // Height of the Container widget
        height: 500,
        // Decoration of the Container widget
        decoration: BoxDecoration(
          // Gradient of the Container widget
          gradient: LinearGradient(
            // Colors of the gradient
            colors: [
              appPrimaryColor.withOpacity(0.2),
              appTextColor.withOpacity(0.2),
            ],
          ),
        ),
        child: IntrinsicHeight(
          // Wraps the Row widget
          child: Row(
            // Cross axis alignment of the Row widget
            crossAxisAlignment: CrossAxisAlignment.center,
            // Main axis alignment of the Row widget
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Expanded(
                // Flex of the Expanded widget
                flex: 2,
                child: Column(
                  // Main axis alignment of the Column widget
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Text(
                      "Deine Job\nwebsite",
                      // Style of the Text widget
                      style: AppTextStyle.regularBold36
                          .copyWith(fontSize: 65, color: blackColor),
                    ),
                    // Vertical space
                    height10,
                  ],
                ),
              ),
              Expanded(
                // Child of the Expanded widget
                child: CircleAvatar(
                  // Radius of the CircleAvatar widget
                  radius: 150,
                  // Background color of the CircleAvatar widget
                  backgroundColor: whiteColor,
                  child: ClipRRect(
                    // Border radius of the ClipRRect widget
                    borderRadius: BorderRadius.circular(200),
                    child: Image.asset(
                      "assets/png/undraw_agreement_aajr@2x.png",
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                ),
              ),
              const Expanded(
                // Flex of the Expanded widget
                flex: 2,
                // Child of the Expanded widget
                child: SizedBox(),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
