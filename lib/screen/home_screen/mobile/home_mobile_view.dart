import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:provider/provider.dart';
import 'package:test_project/constant/app_bar.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';
import 'package:test_project/provider/home_provider.dart';
import 'package:test_project/screen/home_screen/mobile/tab_bar_view/arbeitgeber_view.dart';
import 'package:test_project/screen/home_screen/mobile/tab_bar_view/arbeitnehmer_view.dart';
import 'package:test_project/screen/home_screen/mobile/tab_bar_view/tempor%C3%A4rb%C3%BCro_view.dart';

class HomeMobileView extends StatefulWidget {
  const HomeMobileView({super.key});

  @override
  State<HomeMobileView> createState() => _HomeMobileViewState();
}

class _HomeMobileViewState extends State<HomeMobileView> {
  @override

  /// Builds the widget tree for the HomeMobileView.
  ///
  /// This method is called when the widget needs to be built. It returns a
  /// Scaffold widget which includes an app bar, a bottom navigation bar, and a
  /// body that contains a scrollable column of widgets. The body contains a
  /// consumer widget that listens to changes in the HomeProvider. It builds
  /// different tab views based on the selected tab index.
  Widget build(BuildContext context) {
    // Build the widget tree for the HomeMobileView.
    return Scaffold(
      // Build the app bar.
      appBar: appBar(),
      // Build the bottom navigation bar.
      bottomNavigationBar: _buildBottomNavigationBar(),
      // Build the body of the Scaffold.
      body: _buildBody(context),
    );
  }

  /// Builds the bottom navigation bar.
  ///
  /// Returns a Container widget that includes decoration and a GestureDetector
  /// widget.
  Widget _buildBottomNavigationBar() {
    // Build the bottom navigation bar.
    return Container(
      // Build the decoration.
      decoration: _buildDecoration(),
      // Build the GestureDetector.
      child: _buildGestureDetector(),
    );
  }

  /// Builds the decoration for the bottom navigation bar.
  ///
  /// Returns a BoxDecoration widget.
  BoxDecoration _buildDecoration() {
    // Build the BoxDecoration.
    return const BoxDecoration(
      // Build the border radius.
      borderRadius: BorderRadius.vertical(top: Radius.circular(12)),
      // Build the color.
      color: whiteColor,
      // Build the box shadow.
      boxShadow: [
        BoxShadow(
          color: Colors.black12,
          offset: Offset(-1, 0),
          spreadRadius: 3,
          blurRadius: 6,
        )
      ],
    );
  }

  /// Builds the GestureDetector for the bottom navigation bar.
  ///
  /// Returns a GestureDetector widget.
  Widget _buildGestureDetector() {
    // Build the GestureDetector.
    return GestureDetector(
      // Build the child.
      child: _buildChild(),
    );
  }

  /// Builds the child for the GestureDetector.
  ///
  /// Returns a Container widget.
  Widget _buildChild() {
    // Build the Container.
    return Container(
      // Build the height.
      height: 40,
      // Build the margin.
      margin: const EdgeInsets.all(20.0),
      // Build the width.
      width: double.infinity,
      // Build the constraints.
      constraints: const BoxConstraints(maxWidth: 350),
      // Build the decoration.
      decoration: _buildDecorationForChild(),
      // Build the child.
      child: _buildCenterChild(),
    );
  }

  /// Builds the decoration for the child.
  ///
  /// Returns a BoxDecoration widget.
  BoxDecoration _buildDecorationForChild() {
    // Build the BorderRadius.
    BorderRadius radius = BorderRadius.circular(10);
    // Build the BoxDecoration.
    return BoxDecoration(
      borderRadius: radius,
      gradient: const LinearGradient(colors: [appTextColor, appPrimaryColor]),
    );
  }

  /// Builds the center child for the Container.
  ///
  /// Returns a Center widget.
  Widget _buildCenterChild() {
    // Build the Center widget.
    return Center(
      // Build the child.
      child: Text(
        "Kostenlos Registrieren",
        style:
            AppTextStyle.normalSemiBold15.copyWith(fontWeight: FontWeight.w600),
        textAlign: TextAlign.center,
      ),
    );
  }

  /// Builds the body of the HomeMobileView.
  ///
  /// Returns a Consumer widget that listens to changes in the HomeProvider.
  /// It builds different tab views based on the selected tab index.
  Widget _buildBody(BuildContext context) {
    // Build the Consumer widget.
    return Consumer<HomeProvider>(
      builder: (context, value, child) {
        // Build the body of the Scaffold.
        return SingleChildScrollView(
          child: Column(
            children: [
              // Build the header view.
              _headerView(),
              // Builds the padding widget.
              Padding(
                // The padding of the Padding widget.
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                // Builds the SizedBox widget.
                child: SizedBox(
                  // The height of the SizedBox widget.
                  height: 50,
                  // Builds the SingleChildScrollView widget.
                  child: SingleChildScrollView(
                    // The scroll direction of the SingleChildScrollView widget.
                    scrollDirection: Axis.horizontal,
                    // Builds the Row widget.
                    child: Row(
                      // Builds the list of GestureDetector widgets.
                      children: [
                        // Builds a list of GestureDetector widgets based on
                        // the length of tabBarList.
                        ...List.generate(
                          value.tabBarList.length,
                          // Builds a GestureDetector widget.
                          (index) => GestureDetector(
                            // Called when the GestureDetector is tapped.
                            onTap: () {
                              // Calls the changeTabIndex method of HomeProvider.
                              value.changeTabIndex(index);
                            },
                            // Builds a Container widget.
                            child: Container(
                              // The decoration of the Container widget.
                              decoration: BoxDecoration(
                                // The border radius of the Container widget.
                                borderRadius: BorderRadius.horizontal(
                                  // The left radius of the Container widget.
                                  left: Radius.circular(
                                    index == 0 ? 12 : 0,
                                  ),
                                  // The right radius of the Container widget.
                                  right: Radius.circular(
                                    index == 2 ? 12 : 0,
                                  ),
                                ),
                                // The border of the Container widget.
                                border: Border.all(
                                  color: value.selectedTabIndex == index
                                      ? tabBarColor
                                      : borderColor,
                                ),
                                // The color of the Container widget.
                                color: value.selectedTabIndex == index
                                    ? tabBarColor
                                    : Colors.transparent,
                              ),
                              // The height of the Container widget.
                              height: 40,
                              // The width of the Container widget.
                              width: 160,
                              // Builds the Center widget.
                              child: Center(
                                // Builds a Text widget.
                                child: Text(
                                  // The text of the Text widget.
                                  value.tabBarList[index],
                                  // The style of the Text widget.
                                  style: AppTextStyle.regularBold15.copyWith(
                                    // The color of the Text widget.
                                    color: value.selectedTabIndex == index
                                        ? selectedTabIndex
                                        : appTextColor,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              // Adds 20 vertical space.
              customHeight(20),
              // Build the tab view.

              _buildTabView(value),
            ],
          ),
        );
      },
    );
  }

  /// Builds the tab view based on the selected tab index.
  ///
  /// Returns one of three tab views based on the selected tab index.
  Widget _buildTabView(HomeProvider value) {
    // Build the tab view.
    return [
      const ArbeitnehmerTabView(),
      const ArbeitgeberTabView(),
      const TemporarburoTabView(),
    ][value.selectedTabIndex];
  }

  /// Builds the header view of the HomeMobileView.
  ///
  /// Returns a [ClipPath] widget that clips a [Container] with a [BoxDecoration]
  /// gradient and a [Column] widget containing a [Text] widget and an [SvgPicture] widget.
  Widget _headerView() {
    // The ClipPath widget clips a Container with a gradient background.
    return ClipPath(
      // The WaveClipperTwo class defines the shape of the clip path.
      clipper: WaveClipperTwo(),
      child: Container(
        // The height of the container.
        height: 600,
        // The decoration of the container, which includes a gradient.
        decoration: BoxDecoration(
          gradient: LinearGradient(
            // The colors of the gradient.
            colors: [
              appPrimaryColor.withOpacity(0.2),
              appTextColor.withOpacity(0.2),
            ],
          ),
        ),
        // The child of the container, which is a Column widget.
        child: Column(
          children: [
            // Padding widget that adds padding to the top, left, and right of the Text widget.
            Padding(
              padding: const EdgeInsets.only(
                top: 20.0,
                left: 20.0,
                right: 20.0,
              ),
              // The Text widget that displays the title of the website.
              child: Text(
                'Deine Job website',
                // The style of the Text widget.
                style: AppTextStyle.normalRegularBold42
                    .copyWith(color: blackColor),
                // The text alignment of the Text widget.
                textAlign: TextAlign.center,
              ),
            ),
            // SizedBox widget that wraps the SvgPicture widget.
            SizedBox(
              // The width of the SizedBox widget.
              width: MediaQuery.of(context).size.width,
              // The SvgPicture widget that displays an SVG image.
              child: SvgPicture.asset(
                "assets/svg/undraw_agreement_aajr.svg",
                // The height of the SvgPicture widget.
                height: 450,
                // The fit of the SvgPicture widget.
                fit: BoxFit.fitWidth,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
