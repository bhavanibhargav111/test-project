import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';

class ArbeitnehmerTabView extends StatelessWidget {
  const ArbeitnehmerTabView({super.key});

  @override

  /// Builds the UI for the ArbeitnehmerTabView.
  ///
  /// This widget returns a Column containing a text widget, three step widgets.
  /// The text widget is used to provide instructions to the user. The step widgets
  /// are used to guide the user through the process of finding a new job.
  ///
  /// Parameters:
  ///   - context: the build context of this widget
  ///
  /// Returns:
  ///   - A Column widget containing the necessary widgets.
  @override
  Widget build(BuildContext context) {
    // The main column that contains the UI
    return Column(
      children: [
        // A Padding widget that provides padding to the Text widget
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          // A Text widget that provides instructions to the user
          child: Text(
            "Drei einfache Schritte zu deinem neuen Job",
            // The style of the Text widget
            style: AppTextStyle.normalRegularBold20.copyWith(
              color: grayColor,
            ),
            // The alignment of the Text widget
            textAlign: TextAlign.center,
          ),
        ),
        // A height widget that provides a blank space of 20 pixels
        height20,
        // A step widget that guides the user through the first step
        _stepOne(),
        // A step widget that guides the user through the second step
        _stepTwo(),
        // A step widget that guides the user through the third step
        _stepThree()
      ],
    );
  }

  /// This widget represents the third step of the job search process.
  ///
  /// It displays an image and a text widget. The image is positioned to the right
  /// bottom of the widget, and the text widget is positioned to the left top of
  /// the widget.
  ///
  /// Parameters:
  ///   None
  ///
  /// Returns:
  ///   A Padding widget containing a SizedBox widget. The SizedBox widget contains
  ///   a Stack widget. The Stack widget contains two Positioned widgets.
  Widget _stepThree() {
    // The main column that contains the UI
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: SizedBox(
        height: 270, // The height of the widget
        child: Stack(
          clipBehavior: Clip.none, // The behavior of the widget's clipping area
          children: [
            // The Positioned widget that contains the image
            Positioned(
              right: 20, // The right padding of the widget
              bottom: 0, // The bottom padding of the widget
              child: Image.asset(
                "assets/png/arbeitnehmer_step_three.png", // The image asset path
                height: 144, // The height of the image
              ),
            ),
            // The Positioned widget that contains the text
            Positioned(
              left: 35, // The left padding of the widget
              top: -20, // The top padding of the widget
              child: Row(
                children: [
                  // The Text widget that displays the number and the title of the step
                  Text(
                    "3. ",
                    style: AppTextStyle
                        .normalRegularBold42 // The style of the number
                        .copyWith(fontSize: 130, color: grayColor),
                  ),
                  Text(
                    "Mit nur einem Klick\nbewerben", // The text of the step
                    style:
                        AppTextStyle.normalSemiBold15 // The style of the text
                            .copyWith(color: grayColor),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// A widget representing the first step of the job seeker process.
  ///
  /// It displays an image and a text widget. The image is positioned on the right,
  /// and the text widget is positioned on the left bottom.
  ///
  /// Returns:
  /// A [SizedBox] widget containing a [Stack] widget. The [Stack] widget contains
  /// two [Positioned] widgets.
  Widget _stepOne() {
    return SizedBox(
      height: 254, // The height of the widget
      child: Stack(
        children: [
          // Positioned widget that contains the image
          Positioned(
            right: 20, // The right padding of the widget
            child: Image.asset(
              "assets/png/arbeitnehmer_step_one.png", // The image asset path
              height: 144, // The height of the image
            ),
          ),
          // Positioned widget that contains the text
          Positioned(
            left: 10, // The left padding of the widget
            bottom: 5, // The bottom padding of the widget
            child: RichText(
              text: TextSpan(
                text: "1. ", // The text for the step number
                style: AppTextStyle
                    .normalRegularBold42 // The style of the step number
                    .copyWith(fontSize: 130, color: grayColor),
                children: [
                  TextSpan(
                    text:
                        "Erstellen dein Lebenslauf", // The text for the step title
                    style: AppTextStyle
                        .normalSemiBold15 // The style of the step title
                        .copyWith(color: grayColor),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  /// Widget representing the second step of the job seeker process.
  ///
  /// It displays a wave-clipped container with an image and a text widget.
  /// The container is stacked on top of two other wave-clipped containers.
  ///
  /// Returns:
  /// A [Column] widget containing three [ClipPath] widgets. Each [ClipPath] widget
  /// contains a [Container] widget.
  Widget _stepTwo() {
    return Column(
      children: [
        // Top wave-clipped container with a 50px height and 50% opacity based on the selectedTabIndex
        ClipPath(
          clipper: WaveClipperTwo(reverse: true),
          child: Container(
            height: 50,
            color: selectedTabIndex.withOpacity(0.5),
          ),
        ),
        // Middle container with a height of 300px, 50% opacity based on the selectedTabIndex
        // and a stack containing an image and a text widget
        Container(
          color: selectedTabIndex.withOpacity(0.5),
          height: 300,
          child: Stack(
            alignment: Alignment
                .center, // Aligns the children of the stack in the center
            children: [
              // Positioned widget containing an image on the right bottom
              Positioned(
                right: 30, // Right padding of 30
                bottom: 0, // Bottom padding of 0
                child: Image.asset(
                  "assets/png/arbeitnehmer_step_two.png", // Image asset path
                  height: 144, // Image height of 144px
                ),
              ),
              // Positioned widget containing a text widget on the left top
              Positioned(
                top: 0, // Top padding of 0
                left: 30, // Left padding of 30
                child: RichText(
                  text: TextSpan(
                    text: "2. ", // Text for the step number
                    style: AppTextStyle
                        .normalRegularBold42 // Style for the step number
                        .copyWith(fontSize: 130, color: grayColor),
                    children: [
                      TextSpan(
                        text:
                            "Erstellen dein Lebenslauf", // Text for the step title
                        style: AppTextStyle
                            .normalSemiBold15 // Style for the step title
                            .copyWith(color: grayColor),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        // Bottom wave-clipped container with a 50px height and 50% opacity based on the selectedTabIndex
        ClipPath(
          clipper: WaveClipperOne(),
          child: Container(
            height: 50,
            color: selectedTabIndex.withOpacity(0.5),
          ),
        ),
      ],
    );
  }
}
