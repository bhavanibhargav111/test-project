import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';

class ArbeitgeberTabView extends StatelessWidget {
  const ArbeitgeberTabView({super.key});

  @override

  /// Builds the widget tree for the ArbeitgeberTabView.
  ///
  /// This widget is responsible for displaying the steps to create a new
  /// employer profile.
  ///
  /// The [BuildContext] parameter [context] is used to access the theme
  /// and other properties of the widget tree.
  ///
  /// Returns a [Column] widget that contains a [Padding] widget, a
  /// [Text] widget, and three step widgets [_stepOne(), _stepTwo(),
  /// _stepThree()].
  Widget build(BuildContext context) {
    // The widget tree for displaying the steps to create a new employer profile.
    return Column(
      children: [
        // Adds padding to the left and right of the child widget.
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          // A text widget displaying the instructions to create a new employer profile.
          child: Text(
            "Drei einfache Schritte zu deinem neuen Mitarbeiter",
            // Applies the normal regular bold 20 style to the text.
            style: AppTextStyle.normalRegularBold20.copyWith(
              color: grayColor,
            ),
            // Aligns the text to the center.
            textAlign: TextAlign.center,
          ),
        ),
        // Adds 20 pixels of vertical space.
        height20,
        // The first step widget.
        _stepOne(),
        // The second step widget.
        _stepTwo(),
        // The third step widget.
        _stepThree(),
        // Adds 40 pixels of vertical space.
        customHeight(40),
      ],
    );
  }

  /// The widget for the first step of creating a new employer profile.
  ///
  /// This widget displays an image of a person and a text describing the step.
  /// It uses a [Stack] widget to overlay the image and the text on top of each other.
  ///
  /// Returns a [SizedBox] widget containing a [Stack] widget.
  Widget _stepOne() {
    return SizedBox(
      // Set the height of the sized box.
      height: 254,
      // The stack widget allows to overlay widgets on top of each other.
      child: Stack(
        // Set the clip behavior to Clip.none to avoid clipping the children.
        clipBehavior: Clip.none,
        // The list of widgets to be displayed on top of each other.
        children: [
          // Position the image widget to the right of the stack.
          Positioned(
            right: 20,
            // Display the employer step one image.
            child: Image.asset(
              "assets/png/arbeitnehmer_step_one.png",
              height: 144,
            ),
          ),
          // Position the text widget to the left of the stack and bottom of the image.
          Positioned(
            left: 10,
            bottom: -20,
            // Display the text describing the first step of the employer profile creation.
            child: Row(
              // Set the main axis size to min to make the row as narrow as possible.
              mainAxisSize: MainAxisSize.min,
              // Set the cross axis alignment to center to align the text in the center.
              crossAxisAlignment: CrossAxisAlignment.center,
              // The list of widgets to be displayed in a row.
              children: [
                // Display the number "1. " in bold and semi-bold font.
                Text(
                  "1. ",
                  style: AppTextStyle.normalRegularBold42
                      .copyWith(fontSize: 130, color: grayColor),
                ),
                // Display the text "Erstellen dein\nUnternehmenspofil" in semi-bold font.
                Text(
                  "Erstellen dein\nUnternehmenspofil",
                  style:
                      AppTextStyle.normalSemiBold15.copyWith(color: grayColor),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// Widget for the second step of creating a new employer profile.
  ///
  /// This widget displays a clipped container with a stack of widgets. The stack contains
  /// two positioned widgets, one for the image and the other for the text.
  Widget _stepTwo() {
    return Column(
      children: [
        // A clipped container with a custom clipper and a semi-transparent color.
        ClipPath(
          clipper: WaveClipperTwo(reverse: true),
          child: Container(
            height: 50, // height of the container.
            color: selectedTabIndex.withOpacity(0.5), // semi-transparent color.
          ),
        ),
        // A container with a semi-transparent color and a stack of widgets.
        Container(
          color: selectedTabIndex.withOpacity(0.5), // semi-transparent color.
          height: 300, // height of the container.
          child: Stack(
            alignment: Alignment.center, // alignment of the stack.
            children: [
              // A positioned widget for the employer step two image.
              Positioned(
                right: 30, // right positioning of the widget.
                bottom: 0, // bottom positioning of the widget.
                child: Image.asset(
                  "assets/png/arbeitgeber_step_two.png", // image asset path.
                  height: 189, // height of the image.
                ),
              ),
              // A positioned widget for the text describing the second step of the employer
              // profile creation.
              Positioned(
                top: 10, // top positioning of the widget.
                left: 40, // left positioning of the widget.
                child: RichText(
                  text: TextSpan(
                    text: "2. ", // Text for the number.
                    style: AppTextStyle.normalRegularBold42.copyWith(
                        fontSize: 130,
                        color: grayColor), // Style for the number.
                    children: [
                      TextSpan(
                        text:
                            "Erstellen dein Lebenslauf", // Text for the main text.
                        style: AppTextStyle.normalSemiBold15.copyWith(
                            color: grayColor), // Style for the main text.
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        // A clipped container with a custom clipper and a semi-transparent color.
        ClipPath(
          clipper: WaveClipperOne(),
          child: Container(
            height: 50, // height of the container.
            color: selectedTabIndex.withOpacity(0.5), // semi-transparent color.
          ),
        ),
      ],
    );
  }
}

/// Widget representing the third step of the employer profile creation process.
///
/// It displays a stack of widgets with an image and a text widget.
/// The image and the text are positioned on the stack.
/// The stack is padded and has a fixed height.
Widget _stepThree() {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 20.0),
    child: SizedBox(
      height: 350, // The height of the stack.
      child: Stack(
        clipBehavior: Clip.none, // Allows children to overflow.
        children: [
          // A positioned widget containing the image.
          Positioned(
            right: 20, // Right padding of 20.
            left: 20, // Left padding of 20.
            bottom: 0, // Bottom padding of 0.
            child: Image.asset(
              "assets/png/arbeitgeber_step_three.png", // Image asset path.
              height: 189, // Image height of 189.
            ),
          ),
          // A positioned widget containing the text.
          Positioned(
            top: -10, // Top padding of -10.
            left: 35, // Left padding of 35.
            child: Row(
              children: [
                Text(
                  "3. ", // Text for the step number.
                  style: AppTextStyle.normalRegularBold42.copyWith(
                      fontSize: 130,
                      color: grayColor), // Style for the step number.
                ),
                Text(
                  "Wähle deinen\nneuen Mitarbeiter aus", // Text for the main text.
                  style: AppTextStyle.normalSemiBold15
                      .copyWith(color: grayColor), // Style for the main text.
                ),
              ],
            ),
          ),
        ],
      ),
    ),
  );
}
