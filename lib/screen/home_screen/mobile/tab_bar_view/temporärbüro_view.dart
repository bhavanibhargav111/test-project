// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_custom_clippers/flutter_custom_clippers.dart';
import 'package:test_project/constant/app_colors.dart';
import 'package:test_project/constant/app_style.dart';
import 'package:test_project/constant/static_decoration.dart';

class TemporarburoTabView extends StatelessWidget {
  const TemporarburoTabView({super.key});

  @override

  /// Builds the main widget of the TemporarburoTabView.
  ///
  /// This widget is a Column containing multiple widgets, each
  /// representing a step towards a new Mitarbeiter.
  ///
  /// Parameters:
  /// - [context]: The BuildContext of the widget.
  ///
  /// Returns:
  /// A Column widget containing the steps to a new Mitarbeiter.
  Widget build(BuildContext context) {
    // The main Column widget containing the steps to a new Mitarbeiter.
    return Column(
      children: [
        // A Padding widget that contains a text widget.
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          // A text widget that displays a message to the user.
          child: Text(
            "Drei einfache Schritte zu deinem neuen Mitarbeiter",
            style: AppTextStyle.normalRegularBold20.copyWith(
              color: grayColor,
            ),
            // The text is centered.
            textAlign: TextAlign.center,
          ),
        ),
        // A spacer of 20 height.
        height20,
        // The first step widget.
        _stepOne(),
        // The second step widget.
        _stepTwo(),
        // The third step widget.
        _stepThree(),
        // A spacer of 40 height.
        customHeight(40)
      ],
    );
  }

  /// Builds the widget for the first step of creating a new Mitarbeiter.
  ///
  /// This widget is a Stack containing two Positioned widgets, one
  /// displaying an image and the other displaying text.
  ///
  /// Returns:
  /// A SizedBox widget containing the Stack widget.
  Widget _stepOne() {
    return SizedBox(
      // Set the height of the SizedBox.
      height: 254,
      // Build a Stack widget containing two Positioned widgets.
      child: Stack(
        // Set the clip behavior of the Stack.
        clipBehavior: Clip.none,
        // Build the children of the Stack.
        children: [
          // Build the Positioned widget for the image.
          Positioned(
            // Set the right offset of the image.
            right: 20,
            // Build the Image widget.
            child: Image.asset(
              "assets/png/arbeitnehmer_step_one.png",
              // Set the height of the image.
              height: 144,
            ),
          ),
          // Build the Positioned widget for the text.
          Positioned(
            // Set the left offset of the text.
            left: 10,
            // Set the bottom offset of the text.
            bottom: -20,
            // Build the Row widget containing two Text widgets.
            child: Row(
              // Set the main axis size of the Row.
              mainAxisSize: MainAxisSize.min,
              // Set the cross axis alignment of the Row.
              crossAxisAlignment: CrossAxisAlignment.center,
              // Build the children of the Row.
              children: [
                // Build the first Text widget.
                Text(
                  "1. ",
                  // Set the style of the first Text widget.
                  style: AppTextStyle.normalRegularBold42
                      .copyWith(fontSize: 130, color: grayColor),
                ),
                // Build the second Text widget.
                Text(
                  "Erstellen dein\nUnternehmenspofil",
                  // Set the style of the second Text widget.
                  style:
                      AppTextStyle.normalSemiBold15.copyWith(color: grayColor),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  /// Builds the widget for the second step of creating a new Mitarbeiter.
  ///
  /// This widget is a Column containing three widgets.
  /// The first widget is a ClipPath widget with a WaveClipperTwo widget as its child.
  /// The second widget is a Container widget with a Stack widget as its child.
  /// The third widget is a ClipPath widget with a WaveClipperOne widget as its child.
  ///
  /// The Stack widget contains two Positioned widgets, one displaying an image and the other displaying text.
  ///
  /// Returns:
  /// The Column widget containing the three widgets.
  Widget _stepTwo() {
    return Column(
      children: [
        // The first widget, a ClipPath widget with a WaveClipperTwo widget as its child.
        ClipPath(
          clipper: WaveClipperTwo(reverse: true),
          child: Container(
            height: 50, // Set the height of the Container widget.
            color: selectedTabIndex
                .withOpacity(0.5), // Set the color of the Container widget.
          ),
        ),

        // The second widget, a Container widget with a Stack widget as its child.
        Container(
          color: selectedTabIndex
              .withOpacity(0.5), // Set the color of the Container widget.
          height: 300, // Set the height of the Container widget.
          child: Stack(
            alignment:
                Alignment.center, // Set the alignment of the Stack widget.
            children: [
              // The first Positioned widget, displaying an image.
              Positioned(
                right: 30, // Set the right offset of the image.
                bottom: 10, // Set the bottom offset of the image.
                child: Image.asset(
                  "assets/png/temporarburoView_step_two.png", // Set the path of the image.
                  height: 150, // Set the height of the image.
                ),
              ),

              // The second Positioned widget, displaying text.
              Positioned(
                left: 30, // Set the left offset of the text.
                top: -15, // Set the top offset of the text.
                child: Row(
                  mainAxisSize:
                      MainAxisSize.min, // Set the main axis size of the Row.
                  crossAxisAlignment: CrossAxisAlignment
                      .center, // Set the cross axis alignment of the Row.
                  children: [
                    Text(
                      "2. ", // Display the number of the step.
                      style: AppTextStyle.normalRegularBold42.copyWith(
                          fontSize: 130,
                          color:
                              grayColor), // Set the style of the Text widget.
                    ),
                    Text(
                      "Erhalte Vermittlungs-\nangebot von Arbeitgeber", // Display the text.
                      style: AppTextStyle.normalSemiBold15.copyWith(
                          color:
                              grayColor), // Set the style of the Text widget.
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),

        // The third widget, a ClipPath widget with a WaveClipperOne widget as its child.
        ClipPath(
          clipper: WaveClipperOne(),
          child: Container(
            height: 50, // Set the height of the Container widget.
            color: selectedTabIndex
                .withOpacity(0.5), // Set the color of the Container widget.
          ),
        ),
      ],
    );
  }

  /// Builds the widget for the third step of creating a new Mitarbeiter.
  ///
  /// This widget is a Padding widget containing a SizedBox widget with a Stack widget as its child.
  /// The Stack widget contains two Positioned widgets, one displaying an image and the other displaying text.
  ///
  /// The first Positioned widget displays an image.
  /// The second Positioned widget displays text.
  ///
  /// Returns:
  /// The Padding widget containing the SizedBox widget.
  Widget _stepThree() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 20.0),
      child: SizedBox(
        height: 350, // Set the height of the SizedBox.
        child: Stack(
          clipBehavior: Clip.none, // Set the clip behavior of the Stack.
          children: [
            // The first Positioned widget, displaying an image.
            Positioned(
              right: 20, // Set the right offset of the image.
              left: 20, // Set the left offset of the image.
              bottom: 0, // Set the bottom offset of the image.
              child: Image.asset(
                "assets/png/temporarburoView_step_three.png", // Set the path of the image.
                height: 189, // Set the height of the image.
              ),
            ),
            // The second Positioned widget, displaying text.
            Positioned(
              top: -10, // Set the top offset of the text.
              left: 35, // Set the left offset of the text.
              child: Row(
                children: [
                  Text(
                    "3. ", // Display the number of the step.
                    style: AppTextStyle.normalRegularBold42.copyWith(
                        fontSize: 130,
                        color: grayColor), // Set the style of the Text widget.
                  ),
                  Text(
                    "Vermittlung nach\nProvision oder\nStundenlohn", // Display the text.
                    style: AppTextStyle.normalSemiBold15.copyWith(
                        color: grayColor), // Set the style of the Text widget.
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
