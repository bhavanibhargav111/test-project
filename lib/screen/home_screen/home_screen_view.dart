import 'package:flutter/material.dart';
import 'package:test_project/helper/responsive.dart';
import 'package:test_project/screen/home_screen/desktop/home_desktop_view.dart';
import 'package:test_project/screen/home_screen/mobile/home_mobile_view.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({super.key});

  @override

  /// Builds the [HomeScreen] widget.
  ///
  /// This method builds a widget that displays the appropriate
  /// view based on the device's screen size, either the mobile or
  /// desktop view.
  ///
  /// The [BuildContext] parameter [context] is used to access the
  /// current build context.
  ///
  /// Returns a [Widget] representing the [HomeScreen].
  Widget build(BuildContext context) {
    // Build the appropriate view based on the device's screen size.
    // The view is determined by the [ResponsiveView] widget.
    // If the screen size is mobile, the [HomeMobileView] is displayed.
    // If the screen size is desktop, the [HomeDesktopView] is displayed.
    return const ResponsiveView(
      mobile: HomeMobileView(),
      desktop: HomeDesktopView(),
    );
  }
}
